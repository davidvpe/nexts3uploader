import React, { useEffect, useRef, useState } from 'react'
import styles from './styles.module.css'

interface Props {}

enum DragStatus {
    none = 0,
    dragOver = 1,
}

const FileSelector = (props: Props) => {
    const fileInputRef = useRef<HTMLInputElement>()
    const formRef = useRef<HTMLFormElement>()
    const [status, setStatus] = useState<DragStatus>(DragStatus.none)
    const [wants, setWants] = useState<Boolean>(false)
    const [files, setFiles] = useState<FileList>(null)

    useEffect(() => {
        if (!files || files.length === 0) return
        fileInputRef.current.files = files
        formSubmit()
    }, [files])

    const formSubmit = () => {
        const data = new FormData()

        for (const file of files) {
            data.append('files[]', file, file.name)
        }

        fetch('/api/upload', {
            method: 'POST',
            body: data,
        })
    }

    return (
        <div
            className={status === DragStatus.none ? styles.fscontainer : styles.fscontainerdragged}
            onDrop={async (e) => {
                e.stopPropagation()
                e.preventDefault()
                let files = e.dataTransfer.files
                setFiles(files)
            }}
            onDragOver={(e) => {
                e.stopPropagation()
                e.preventDefault()
                setWants(true)
                if (status != DragStatus.dragOver) setStatus(DragStatus.dragOver)
                console.log(e)
            }}
            onDragLeave={(e) => {
                e.stopPropagation()
                e.preventDefault()
                setWants(true)
                setTimeout(() => {
                    if (wants) setStatus(DragStatus.none)
                }, 500)
            }}
        >
            <h1>Sube y comparte tus imagenes</h1>
            <p>
                Arrastra y suelta donde quieras para comenzar a subir ahora tus imágenes. 32 MB de límite. Enlace directo de imagen,
                miniaturas en BBCode y HTML.
            </p>
            <form ref={formRef} className={styles.buttonContainer}>
                <input ref={fileInputRef} type='file' hidden={true}></input>
                <button>COMIENZA A SUBIR</button>
            </form>
        </div>
    )
}

export default FileSelector
