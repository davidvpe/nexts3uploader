// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import nextConnect from 'next-connect'
import multer from 'multer'
import aws from 'aws-sdk'
import multerS3 from 'multer-s3'
import { NextApiResponse } from 'next'
import path from 'path'

aws.config.update({
    secretAccessKey: process.env.AWS_SECRET,
    accessKeyId: process.env.AWS_ACCESSID,
    region: process.env.AWS_REGION,
})

const s3 = new aws.S3()

const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: process.env.AWS_S3_BUCKET,
        filename: (req, file, cb) => cb(null, Date.now() + path.extname(file.originalname)),
    }),
})

const apiRoute = nextConnect({
    onError(error, req, res: NextApiResponse) {
        res.status(501).json({ error: `Sorry something Happened! ${error.message}` })
    },
    onNoMatch(req, res) {
        res.status(405).json({ error: `Method '${req.method}' Not Allowed` })
    },
})

apiRoute.use(upload.array('files[]'))

apiRoute.post((req, res) => {
    res.status(200).json({ data: 'success' })
})

export default apiRoute

export const config = {
    api: {
        bodyParser: false, // Disallow body parsing, consume as stream
    },
}
