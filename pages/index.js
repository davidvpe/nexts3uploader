import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import FileSelector from '../components/FileSelector'

export default function Home() {
    return (
        <>
            <FileSelector />
        </>
    )
}
